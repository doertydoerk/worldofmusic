# World Of Music Search (WOMS)

*World of Music Search* is a search engine that allows to search music records by certain criteria. 
The only feature currently available is to search music record data provided as an XML file by *Musicmoz*. 
The search looks for records released Jan 1st, 2001 with more than 10 tracks. The result will 
also be provided as a XML file in the following format:

```xml
<matchingReleases>
  <release>
    <name>Cutting Edge</name>
    <releaseDate>1997.08.20</releaseDate>
    <trackCount>15</trackCount>
  </release>
</matchingReleases>
``` 

While it was not a requirement I decided to make the release date also a part of the output.

### Running the program
You may launch the search providing the path to the data source and/or the output path:
```shell
$ ./wom --in=absolutePath/filename.xml --out=absolutePath/filename.xml
```   

In case no arguments are provided the defaults will be used:
```
input = "./musicmoz.xml"
output = "./releases.xml"
````

If those files don't exist an error is logged to the console and the program exists with exit code `1`.

### Architectural considerations
As per the briefing, the program is written in a way that it can be extended pretty easily. For instance,
to add a new search but to keep the current one, you can just implement a new search and have it comply 
to the `Search` interface. To select one of the searches a new launch parameter could be added. The same goes for 
different data sources or destination. 

It is also possible to turn the whole application into a web app with just a few lines of new code and leaving 
the existing code untouched.

Most DDD apps also have some shared lower level functionality, which is supposed to go into a folder called 
infrastructure. For an application as simple as this one, I could not think of anything to be shared between modules. 
Or the functionality was too specific to the module or implementation to be shared.

### Terminology
I used the terms `source` and `sink` to help illustrate the flow of data. That is, data should always flow from source to sink.
The data processing happens along the way.
  