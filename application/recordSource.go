package application

import "gitlab.com/doertydoerk/WorldOfMusic/domain"

type (
	// RecordSource is any source of data on music records
	RecordSource interface {
		// ImportRecords imports records from a source and returns a slice of domain.Release
		ImportRecords() ([]domain.Release, error)
	}
)
