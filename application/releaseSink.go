package application

import "gitlab.com/doertydoerk/WorldOfMusic/domain"

// ReleaseSink is any destination to send a search result of type []Release to
type ReleaseSink interface {

	// ExportReleases to be implemented by a sink that handles a search result of type []release
	// It returns an error if handling of []Release failed or nil upon success
	ExportReleases(r []domain.Release) error
}
