package application

import "gitlab.com/doertydoerk/WorldOfMusic/domain"

// Search defines a search of Releases bases on whatever criteria
type Search interface {

	// Execute performs the search according to its implementation
	Execute(source []domain.Release) []domain.Release
}
