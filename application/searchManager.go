package application

import (
	"errors"
)

type (
	// SearchManager describes the interface any SearchManger has to implement
	SearchManager interface {
		Search(search Search, source RecordSource, sink ReleaseSink) error
	}

	// The DefaultSearchManager that drives the workflow.
	DefaultSearchManager struct{}
)

// Search takes a RecordSource, a ReleaseSink as the actual search as parameters. Upon success it return error = nil.
// Otherwise an error is returned.
func (m DefaultSearchManager) Search(search Search, source RecordSource, sink ReleaseSink) error {
	input, err := source.ImportRecords()

	if err != nil {
		return err
	}

	// have search execute the search
	output := search.Execute(input)

	// have sink process the search result
	if len(output) > 0 {
		return sink.ExportReleases(output)
	}

	return errors.New("search returned no matches")
}
