package application

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/doertydoerk/WorldOfMusic/interfaces"
	"gitlab.com/doertydoerk/WorldOfMusic/mocks"
	"gitlab.com/doertydoerk/WorldOfMusic/testData"
	"testing"
)

func Test_PerformSearch_Success(t *testing.T) {
	input := testData.Releases()
	sourceMock := &mocks.MockSource{}
	sourceMock.On("ImportRecords").Return(input, nil)
	sinkMock := &mocks.MockSink{}
	sinkMock.On("ExportReleases", mock.Anything).Return(nil)

	sut := DefaultSearchManager{}
	err := sut.Search(interfaces.DefaultSearch{}, sourceMock, sinkMock)
	assert.NoError(t, err)
}
