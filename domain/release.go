package domain

// Release represent a record or album
type Release struct {
	// TrackCount is the number of tracks on record
	TackCount int
	//Date is the date when the record was released
	Date string
	// Name is the name/title of the record
	Name string
}
