package interfaces

import (
	"fmt"
	"gitlab.com/doertydoerk/WorldOfMusic/domain"
	"strconv"
)

// DefaultSearch implements the actual search as outlined in the briefing
type DefaultSearch struct{}

// Execute performs the search as outlined in the briefing
func (a DefaultSearch) Execute(source []domain.Release) []domain.Release {
	var result []domain.Release

	for _, rel := range source {
		if rel.TackCount <= 10 {
			continue
		}

		year, err := a.yearFromDate(rel.Date)
		if err != nil || year >= 2001 {
			continue
		}
		result = append(result, rel)
	}

	fmt.Printf("> Found %d matches\n", len(result))
	return result
}

func (a DefaultSearch) yearFromDate(date string) (int, error) {
	year, err := strconv.Atoi(date[:4])

	if err != nil {
		return 0, err
	}

	return year, nil
}
