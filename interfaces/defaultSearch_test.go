package interfaces

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/doertydoerk/WorldOfMusic/testData"
	"testing"
)

// We want to have a list of releases with more than
// 10 tracks and a release date before 01/01/2001.
func Test_RunSearch(t *testing.T) {
	actual := DefaultSearch{}.Execute(testData.Releases())
	assert.Equal(t, 1, len(actual))
	assert.Equal(t, "2000.12.31", actual[0].Date)
	assert.Equal(t, 11, actual[0].TackCount)

}

func Test_YearFromDate(t *testing.T) {
	year, err := DefaultSearch{}.yearFromDate("2000.12.31")
	assert.NoError(t, err)
	assert.Equal(t, 2000, year)

	_, err = DefaultSearch{}.yearFromDate("200.12.31")
	assert.EqualError(t, err, "strconv.Atoi: parsing \"200.\": invalid syntax")
}
