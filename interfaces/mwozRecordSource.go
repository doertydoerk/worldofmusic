package interfaces

import (
	"encoding/xml"
	"gitlab.com/doertydoerk/WorldOfMusic/domain"
	"io/ioutil"
	"os"
)

// MwozRecordSource allows to read record data from an musicmoz XML file
type (
	MwozRecordSource struct {
		File string
	}

	mwozRecords struct {
		Records []mwozRecord `xml:"record"`
	}
	mwozRecord struct {
		// Title of the record
		Title string `xml:"title"`
		// Name  of the record
		Name string `xml:"name"`
		// Genre  of the record
		Genre string `xml:"genre"`
		// Releasedate  of the record
		Releasedate string `xml:"releasedate"`
		// Label of the record
		Label string `xml:"label"`
		// Tracklisting is a array needed due to the XML structure
		Tracklisting []Listing `xml:"tracklisting"`
	}

	// Listing is a struct that holds an array of all tracks
	Listing struct {
		Track []string `xml:"track"`
	}
)

// ImportRecords import musicmoz records from a file and returns a slice of domain.Release
// It takes the path including the filename as an argument
func (m *MwozRecordSource) ImportRecords() ([]domain.Release, error) {
	var releases []domain.Release
	mwoz, err := m.readSourceFile(m.File)
	if err != nil {
		return releases, err
	}

	releases = m.mapToRelease(mwoz.Records)
	if err != nil {
		return releases, err
	}

	return releases, nil
}

func (m MwozRecordSource) readSourceFile(file string) (mwozRecords, error) {
	var records mwozRecords

	xmlFile, err := os.Open(file)

	if err != nil {
		return records, err
	}

	defer xmlFile.Close()

	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		return records, err
	}

	xml.Unmarshal(byteValue, &records)

	return records, nil
}

func (m MwozRecordSource) mapToRelease(records []mwozRecord) []domain.Release {
	var releases []domain.Release

	for _, r := range records {

		release := domain.Release{
			TackCount: len(r.Tracklisting[0].Track),
			Date:      r.Releasedate,
			Name:      r.Name,
		}

		releases = append(releases, release)
	}

	return releases
}
