package interfaces

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_ImportRecords(t *testing.T) {
	sut := MwozRecordSource{
		File: "../musicmoz.xml",
	}
	result, err := sut.ImportRecords()
	assert.NoError(t, err)
	assert.Equal(t, 233, len(result))
}

func Test_ReadSourceFile(t *testing.T) {
	result, err := MwozRecordSource{}.readSourceFile("../musicmoz.xml")
	assert.NoError(t, err)
	assert.True(t, len(result.Records) == 233, "Length should be 233 but is %d", len(result.Records))
}

func Test_MapToRelease(t *testing.T) {
	list := Listing{[]string{"Track 1", "Track 2"}}

	mr := mwozRecord{
		Name:         "Records Name",
		Releasedate:  "2019.04.27",
		Tracklisting: []Listing{list},
	}

	result := MwozRecordSource{}.mapToRelease([]mwozRecord{mr})
	assert.Equal(t, "Records Name", result[0].Name)
	assert.Equal(t, "2019.04.27", result[0].Date)
	assert.Equal(t, 2, result[0].TackCount)
}
