package interfaces

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/doertydoerk/WorldOfMusic/domain"
	"io/ioutil"
)

type (
	// The top level of the to be written XML file
	matchingReleases struct {
		// Releases is the list of matching releases to be listed in the XML file
		Releases []XMLRelease `xml:"release"`
	}

	// XMLRelease is a matching release in the desired XML format
	XMLRelease struct {
		Name        string `xml:"name"`
		ReleaseDate string `xml:"releaseDate"`
		TrackCount  int    `xml:"trackCount"`
	}

	// XMLFileReleaseSink is the type to use to have a slice of Release written out to a file.
	XMLFileReleaseSink struct {
		// File defines the destination the file should be written to
		File string
	}
)

// ExportReleases takes a slice of Release, translates it to XML and has it written out to a file
func (x *XMLFileReleaseSink) ExportReleases(r []domain.Release) error {
	xmlRel := x.mapToXMLFileRelease(r)

	err := x.writeXMLtoFile(xmlRel, x.File)
	if err != nil {
		return err
	}

	return nil
}

func (x XMLFileReleaseSink) writeXMLtoFile(m matchingReleases, file string) error {
	data, err := xml.MarshalIndent(m, "", " ")

	if err != nil {
		return err
	}

	data = []byte(xml.Header + string(data))

	err = ioutil.WriteFile(file, data, 0644)
	if err != nil {
		return err
	}

	fmt.Printf("> Writing result to %s\n", file)
	return nil
}

func (x XMLFileReleaseSink) mapToXMLFileRelease(r []domain.Release) matchingReleases {
	var xmlReleases []XMLRelease

	for _, xmlRel := range r {
		xmlRel := XMLRelease{
			Name:        xmlRel.Name,
			ReleaseDate: xmlRel.Date,
			TrackCount:  xmlRel.TackCount,
		}
		xmlReleases = append(xmlReleases, xmlRel)

	}

	return matchingReleases{Releases: xmlReleases}
}
