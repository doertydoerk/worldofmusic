package interfaces

import (
	"github.com/integralist/go-findroot/find"
	"github.com/stretchr/testify/assert"
	"gitlab.com/doertydoerk/WorldOfMusic/domain"
	"gitlab.com/doertydoerk/WorldOfMusic/testData"
	"os"
	"testing"
)

func cleanUpResult() {
	root, _ := find.Repo()
	path := root.Path + "../releases.xml"

	if _, err := os.Stat(path); err == nil {
		_ = os.Remove(path)
	}
}

func Test_MapRelToXMLRel(t *testing.T) {
	validRel := domain.Release{
		Date:      "2000.12.31",
		TackCount: 11,
	}
	releases := []domain.Release{validRel}
	matches := XMLFileReleaseSink{}.mapToXMLFileRelease(releases)
	assert.Equal(t, 1, len(matches.Releases))
}

func Test_WriteXML2File(t *testing.T) {
	cleanUpResult()

	x1 := XMLRelease{
		TrackCount: 11,
		Name:       "Track 1",
	}

	x2 := XMLRelease{
		TrackCount: 14,
		Name:       "Track 2",
	}

	match := matchingReleases{
		Releases: []XMLRelease{x1, x2},
	}
	err := XMLFileReleaseSink{}.writeXMLtoFile(match, "../releases.xml")
	assert.NoError(t, err)
	assert.FileExists(t, "../releases.xml", "file not found")
}

func Test_ExportReleases(t *testing.T) {
	cleanUpResult()
	sink := XMLFileReleaseSink{File: "../releases.xml"}
	err := sink.ExportReleases(testData.Releases())
	assert.NoError(t, err)
	assert.FileExists(t, "../releases.xml", "file not found")
}
