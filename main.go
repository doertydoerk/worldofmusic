package main

import (
	"fmt"
	"gitlab.com/doertydoerk/WorldOfMusic/application"
	"gitlab.com/doertydoerk/WorldOfMusic/interfaces"
	"os"
	"strings"
)

var (
	input  = "./musicmoz.xml"
	output = "./releases.xml"
)

func parseLaunchArguments(args []string) {
	for _, v := range args {
		if strings.HasPrefix(v, "--in=") {
			input = strings.TrimPrefix(v, "--in=")
		}
		if strings.HasPrefix(v, "--out=") {
			output = strings.TrimPrefix(v, "--out=")
		}
	}

}

func main() {
	parseLaunchArguments(os.Args)

	source := &interfaces.MwozRecordSource{
		File: input,
	}

	sink := &interfaces.XMLFileReleaseSink{
		File: output,
	}

	search := interfaces.DefaultSearch{}

	searchMan := application.DefaultSearchManager{}
	err := searchMan.Search(search, source, sink)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
