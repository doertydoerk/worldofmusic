package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_ParseLaunchArguments(t *testing.T) {
	parseLaunchArguments([]string{"--in=new input", "--out=new output"})
	assert.Equal(t, "new input", input)
	assert.Equal(t, "new output", output)
}
