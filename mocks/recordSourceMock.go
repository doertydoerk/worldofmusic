package mocks

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/doertydoerk/WorldOfMusic/domain"
)

// MockSource is a mock of a ReleaseSource
type MockSource struct {
	mock.Mock
}

// ImportRecords mock
func (m *MockSource) ImportRecords() ([]domain.Release, error) {
	args := m.Called()
	return args.Get(0).([]domain.Release), args.Error(1)
}
