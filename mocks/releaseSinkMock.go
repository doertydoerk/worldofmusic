package mocks

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/doertydoerk/WorldOfMusic/domain"
)

// MockSink is a mock of a ReleaseSink
type MockSink struct {
	mock.Mock
}

// ExportReleases mock
func (s *MockSink) ExportReleases(r []domain.Release) error {
	args := s.Called(r)
	return args.Error(0)
}
