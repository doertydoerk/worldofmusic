package testData

import "gitlab.com/doertydoerk/WorldOfMusic/domain"

var (
	validRel = domain.Release{
		Date:      "2000.12.31",
		TackCount: 11,
		Name:      "validRel",
	}
	allWrong = domain.Release{
		Date:      "2001.01.01",
		TackCount: 10,
		Name:      "allWrong",
	}
	dateWrong = domain.Release{
		Date:      "2001.01.01",
		TackCount: 11,
		Name:      "dateWrong",
	}
	tracksWrong = domain.Release{
		Date:      "2000.12.31",
		TackCount: 10,
		Name:      "tracksWrong",
	}
	invalidDateFormat = domain.Release{
		Date:      "200.12.31",
		TackCount: 10,
		Name:      "invalidDateFormat",
	}
)

// Releases is test data to be used on test that need all possibilities
func Releases() []domain.Release {
	return []domain.Release{validRel, allWrong, dateWrong, tracksWrong, invalidDateFormat}
}
